import subprocess
import time
from sms_processor import process_new_sms  # Import the process_new_sms function from sms_processor.py
from mms_processor import process_new_mms

# Global variables to store the IDs of the last processed messages
last_processed_sms_id = 0
last_processed_mms_id = 0


def process_messages(output, last_processed_id):
    lines = output.strip().split('\n')
    messages = []
    for line in lines:
        parts = line.strip().split(', ')
        if len(parts) < 3:
            print("Waiting for incoming messages...")
            continue
        
        message = {
            '_id': int(parts[0].split('=')[1]),
            'address': parts[1].split('=')[1],
            'body': parts[2].split('=')[1]
        }

        messages.append(message)

    new_messages = [msg for msg in messages if msg['_id'] > last_processed_id]
    return messages, new_messages

def monitor_messages():
    """Monitor incoming MMS and SMS messages."""
    global last_processed_sms_id, last_processed_mms_id

    adb_sms_command = "adb shell content query --uri content://sms/ --projection _id,address,body"
    adb_mms_command = "adb shell content query --uri content://mms/part --projection _id,mid,_data"

    try:
        # Execute adb commands at startup
        process_sms = subprocess.Popen(adb_sms_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output_sms, _ = process_sms.communicate()
        output_sms = output_sms.decode('utf-8')
        sms_messages, _ = process_messages(output_sms, last_processed_sms_id)

        process_mms = subprocess.Popen(adb_mms_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output_mms, _ = process_mms.communicate()
        output_mms = output_mms.decode('utf-8')
        mms_messages, _ = process_messages(output_mms, last_processed_mms_id)

        # Find the message with the highest _id for SMS and MMS
        if sms_messages:
            last_processed_sms_id = max([msg['_id'] for msg in sms_messages])
        if mms_messages:
            last_processed_mms_id = max([msg['_id'] for msg in mms_messages])

    except subprocess.CalledProcessError as e:
        print(f"Error executing ADB command at startup: {e}")

    while True:
        try:
            # Execute adb commands
            process_sms = subprocess.Popen(adb_sms_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output_sms, _ = process_sms.communicate()
            output_sms = output_sms.decode('utf-8')
            sms_messages, new_sms_messages = process_messages(output_sms, last_processed_sms_id)

            process_mms = subprocess.Popen(adb_mms_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output_mms, _ = process_mms.communicate()
            output_mms = output_mms.decode('utf-8')
            mms_messages, new_mms_messages = process_messages(output_mms, last_processed_mms_id)

            if sms_messages:
                last_processed_sms_id = max([msg['_id'] for msg in sms_messages])
                if new_sms_messages:
                    process_new_sms(new_sms_messages)  # Call process_new_sms function for new SMS messages

            if mms_messages:
                last_processed_mms_id = max([msg['_id'] for msg in mms_messages])
                if new_mms_messages:
                    process_new_mms(new_mms_messages)  # Call process_new_sms function for new MMS messages

            # Sleep for 1 second before checking again
            time.sleep(1)

        except subprocess.CalledProcessError as e:
            print(f"Error executing ADB command: {e}")
        except KeyboardInterrupt:
            print("Monitoring stopped.")
            break

if __name__ == "__main__":
    print("Message Monitor running...")
    monitor_messages()
