# sms_processor.py

import subprocess

def send_sms(phone_number, message):
    """Send an SMS message to the specified phone number."""
    adb_command = (
        f"adb shell service call isms 5 i32 0 s16 'com.android.mms.service' "
        f"s16 'null' s16 '{phone_number}' s16 'null' s16 '\"{message}\"' "
        f"s16 'null' s16 'null' i32 0 i64 0"
    )

    try:
        subprocess.run(adb_command, shell=True, check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        print(f"SMS sent to {phone_number}")
    except subprocess.CalledProcessError as e:
        print(f"Error sending SMS to {phone_number}: {e}")


def send_welcome_sms(phone_number):
    """Send a welcome SMS to a new phone number."""
    welcome_message = f"Welcome to Photobug\n\nYou have 3 photos remaining. Please text your photo to me and watch me print it!\n\nText 'help' to access our menu."
    send_sms(phone_number, welcome_message)

def register_phone_number(phone_number, prints):
    """Register a new phone number with prints remaining."""
    with open("numbers.txt", "a") as file:
        file.write(f"{phone_number}, {prints}\n")

def process_new_sms(messages):
    """Process new SMS messages."""
    if not messages:
        print("No new SMS messages to process.")
        return

    numbers = read_numbers_file()
    new_registration = False

    for message in messages:
        phone_number = message['address']
        body = message['body']

        # Always print the SMS details
        print(f"SMS - Phone Number: {phone_number}, Body: {body}")

        if body.lower().strip() == 'bug':
            # Check if the phone number is already registered
            if phone_number not in numbers:
                # Register the phone number with default prints
                register_phone_number(phone_number, 3)  # Assuming 3 prints as default
                new_registration = True
                
        elif body.lower().strip() == 'help':
            # Send the menu message back to the user
            menu_message = (
                "Photobug Menu:\n\n"
                "help: prints this menu\n"
                "prints: get remaining prints"
            )
            send_sms(phone_number, menu_message)
        elif body.lower().strip() == 'prints':
            # Check if the phone number is in numbers.txt
            if phone_number in numbers:
                # Get the remaining prints for the phone number
                remaining_prints = numbers[phone_number]
                prints_message = f"You have {remaining_prints} prints left."
                send_sms(phone_number, prints_message)
        else:
            # Do nothing for other messages
            pass

    # Send welcome message if it's a new registration
    if new_registration:
        send_welcome_sms(phone_number)

def read_numbers_file():
    """Read numbers and prints from numbers.txt."""
    numbers = {}
    file_path = "numbers.txt"

    try:
        with open(file_path, "r") as file:
            for line in file:
                parts = line.strip().split(",")
                if len(parts) == 2:
                    phone_number = parts[0].strip()
                    num_prints = int(parts[1].strip())
                    numbers[phone_number] = num_prints
    except FileNotFoundError:
        # Create numbers.txt with default values if it doesn't exist
        with open(file_path, "w") as file:
            pass  # Do nothing, no header line needed

    return numbers

if __name__ == "__main__":
    process_new_sms([])
