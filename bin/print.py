import os
import subprocess
from PIL import Image

def get_image_orientation(image_path):
    """
    Determine the orientation of the image.
    """
    img = Image.open(image_path)
    width, height = img.size
    return "portrait" if height > width else "landscape"

def print_image(image_path, printer_name='YOUR_PRINTER_NAME'):
    """
    Print the image using lpr command with orientation adjustment if needed.
    """
    orientation = get_image_orientation(image_path)
    print_command = f'lpr -o orientation-requested=4 -P {printer_name} {image_path}' if orientation == "portrait" else f'lpr -P {printer_name} {image_path}'
    subprocess.run(print_command, shell=True)

if __name__ == "__main__":
    input_image_path = input("Enter the path to the image file: ")
    printer_name = "YOUR_PRINTER_NAME"  # Replace with your printer name

    # Print the image
    print_image(input_image_path, printer_name)
