import subprocess
import sqlite3
import os

def print_file_with_lpr(filename):
    try:
        # Use subprocess to execute the lpr command with the filename
        subprocess.run(["lpr", filename])
        print(f"File '{filename}' sent to printer.")
    except subprocess.CalledProcessError as e:
        print(f"Error printing file '{filename}': {e}")


def print_image(msg_id):
    # Define the path of the folder to be tarred
    folder_path = "/data/user_de/0/com.android.providers.telephony/app_parts/"

    # Define the path where you want to store the tar file on the device
    tar_path_on_device = "/sdcard/app_parts.tar"

    # Define the path where you want to store the pulled tar file locally
    local_tar_path = "app_parts.tar"

    # Define the path where you want to extract the contents of the tar file locally
    extraction_path = "photos/"

    try:
        subprocess.run(["mkdir", "-p", "photos"], check=True)
        print(f"Folder '{folder_path}' created successfully.")
    except subprocess.CalledProcessError:
        print(f"Folder '{folder_path}' creation failed.")

    try:
        # Run the adb shell command to tar the folder
        subprocess.run(["adb", "shell", "su", "-c", "tar", "-cf", tar_path_on_device, "-C", folder_path, "."], check=True)
        print(f"Folder {folder_path} tarred successfully to {tar_path_on_device}.")

        # Run the adb pull command to pull the tar file to the local directory
        subprocess.run(["adb", "pull", tar_path_on_device, local_tar_path], check=True)
        print(f"Tar file pulled from {tar_path_on_device} to {local_tar_path}.")

        # Run the tar command to extract the contents of the tar file locally
        subprocess.run(["tar", "-xf", local_tar_path, "-C", extraction_path], check=True)
        print(f"Tar file extracted to {extraction_path} successfully.")

        # Delete the local tar file
        os.remove(local_tar_path)

        # Connect to the mmssms.db to get _data from part table
        conn = sqlite3.connect('mmssms.db')
        cursor = conn.cursor()

        # SQL query to get _data from part table where mid = msg_id
        sql_query = f"SELECT _data FROM part WHERE mid = {msg_id}"

        try:
            # Execute the SQL query
            cursor.execute(sql_query)

            # Fetch the result
            result = cursor.fetchone()

            if result:
                data = result[0]
                # Extract the filename from the full path
                filename = os.path.basename(data)

                # Print the filename in the photos directory
                photo_path = os.path.join(extraction_path, filename)
                print(f"Printing: {photo_path}...")
                print_file_with_lpr(photo_path)

            else:
                print(f"No data found for msg_id {msg_id}")

        except sqlite3.Error as e:
            print(f"SQLite error: {e}")

        finally:
            # Close the database connection
            conn.close()

    except subprocess.CalledProcessError as e:
        print(f"Error executing adb command: {e}")

def process_new_mms(messages):
    """Process new MMS messages."""

    # Run the adb shell command to copy mmssms.db to /sdcard/
    try:
        subprocess.run(["adb", "shell", "su", "-c", "cp", "/data/data/com.android.providers.telephony/databases/mmssms.db", "/sdcard/"], check=True)
        print("mmssms.db copied to /sdcard/ successfully.")
    except subprocess.CalledProcessError as e:
        print(f"Error copying mmssms.db: {e}")

    # Run the adb pull command to pull mmssms.db to the local directory
    try:
        subprocess.run(["adb", "pull", "/sdcard/mmssms.db"], check=True)
        print("mmssms.db pulled to local directory successfully.")
    except subprocess.CalledProcessError as e:
        print(f"Error pulling mmssms.db: {e}")

    # Establish connection to the pulled mmssms.db
    conn = sqlite3.connect('mmssms.db')
    cursor = conn.cursor()

    for message in messages:
        # Your processing logic for MMS messages goes here
        print(f"MMS - ID: {message['_id']}, Address: {message['address']}, Body: {message['body']}")

        msg_id = message['_id']

        msg_addr = message['address']
        # Define the SQL query to select msg_id, address, and type from the addr table where type = 137
        sql_query = "SELECT msg_id, address, type FROM addr WHERE type = 137"

        try:
            # Execute the SQL query
            cursor.execute(sql_query)
    
            # Fetch all results
            results = cursor.fetchall()

            # Print the results
            for result in results:
                msg_id, address, message_type = result
                if int(msg_addr) == int(msg_id):
                    # Check if numbers.txt exists, if not, create with default values
                    if not os.path.exists('numbers.txt'):
                        with open('numbers.txt', 'w') as f:
                            pass
                            # Write default values, assuming 5 prints allowed initially
                            # Add more lines as needed

                    # Open numbers.txt to read the remaining prints
                    with open('numbers.txt', 'r') as f:
                        lines = f.readlines()
                        for index, line in enumerate(lines):
                            # Split the line into address and prints_left
                            addr, prints_left = line.strip().split(', ')
                            if addr == address:
                                prints_left = int(prints_left)
                                if prints_left > 0:
                                    prints_left -= 1
                                    print(f"Message ID: {msg_id}, Address: {address}, Type: {message_type}")
                                    print(f"Remaining prints for {addr}: {prints_left}")
                                    # Update the prints_left value in the lines list
                                    lines[index] = f"{addr}, {prints_left}\n"

                                    # Finally, print
                                    print_image(msg_id)
                                else:
                                    print("No more prints left for {address}")
                                break

                    # Write the updated prints_left back to numbers.txt
                    with open('numbers.txt', 'w') as f:
                        for line in lines:
                            f.write(line)

        except sqlite3.Error as e:
            print(f"SQLite error: {e}")

    # Close the database connection outside of the loop
    conn.close()
