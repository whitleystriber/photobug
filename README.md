# Photobug
## Digital Moments into Tangible Memories

PhotoBug, turning your digital memories into tangible keepsakes. Our service specializes in transforming your digital photos into high-quality prints that you can cherish for years to come.

Photobug will scan for incoming SMS/MMS messages and print the data through std:out. If Photobug receives an SMS, it will parse the body to determine what to do with that information.

## Message Structure

PhotoBug responds to the following commands:

- **bug**: Register the user into the numbers.txt file.
- **prints**: Send the sender information about how many prints they have left.
- **help**: Display the menu to the sender for assistance.

## Usage

1. **Registering as a User**:
   - Send a message containing the command "bug" to register yourself into our system.

2. **Checking Your Prints**:
   - To inquire about the number of prints you have left, simply send the command "prints".

3. **Accessing Help**:
   - If you need assistance or want to see the available commands, send the command "help" to get the menu.

## Features

- Reads amd prints incoming MMS images.
- Automatic scaling and rotation of photos
- Integrated SMS registration and menu system
- Automatic printing amd spooler
- Error handling
- Printing limits per phone number
- Logging

## Installation

Photobug requires the following dependencies:

```
python3.11
Manjaro Linux (preferred)
```

Run the following:

```
cd bin
python main.py
```

## License

Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License

By exercising the Licensed Rights (defined below), You accept and agree to be bound by the terms and conditions of this Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License ("Public License"). To the extent this Public License may be interpreted as a contract, You are granted the Licensed Rights in consideration of Your acceptance of these terms and conditions, and the Licensor grants You such rights in consideration of benefits the Licensor receives from making the Licensed Material available under these terms and conditions.
